// Problem 2:

//     Using promises and the fs module, do the following:
//         1. Read the given file lipsum.txt
//         2. Convert the content to uppercase & write to a new file. 
//            Store the name of the new file in filenames.txt
//         3. Read the new file and convert it to lower case. Then split the contents into sentences. 
//            Then write it to a new file. Store the name of the new file in filenames.txt
//         4. Read the new files, sort the content, write it out to a new file. 
//            Store the name of the new file in filenames.txt
//         5. Read the contents of filenames.txt and 
//            delete all the new files that are mentioned in that list simultaneously.


const fs = require("fs")
const path = require("path")



function readManipulateLipsum(lipsumFile, filenames) {

  fs.promises.readFile(path.join(__dirname, lipsumFile))
    .then((data) => {
      return data.toString().toUpperCase()
    })
    .then((upperData) => {
      return fs.promises.writeFile(path.join(__dirname, "upperLipsum.txt"), upperData)
    })
    .then(() => {
      return fs.promises.writeFile(path.join(__dirname, filenames), "upperLipsum.txt\n")
    })
    .then(() => {
      return fs.promises.readFile(path.join(__dirname, "upperLipsum.txt"))
    })
    .then((data) => {
      let processedData = data.toString().toLowerCase().split(".").join("\n");
      return fs.promises.writeFile(path.join(__dirname, "lowerLipsum.txt"), processedData)
    })
    .then(() => {
      return fs.promises.appendFile(path.join(__dirname, filenames), "lowerLipsum.txt\n")
    })
    .then(() => {
      return fs.promises.readFile(path.join(__dirname, "upperLipsum.txt"))
    })
    .then((data) => {
      let sortedUpperData = data.toString().split(" ").sort().join(" ")
      return fs.promises.writeFile(path.join(__dirname, "sortedUpperLipsum.txt"), sortedUpperData)
    })
    .then(() => {
      return fs.promises.appendFile(path.join(__dirname, filenames), "sortedUpperLipsum.txt\n")
    })
    .then(() => {
      return fs.promises.readFile(path.join(__dirname, "lowerLipsum.txt"))
    })
    .then((data) => {
      let sortedLowerData = data.toString().split(" ").sort().join(" ")
      return fs.promises.writeFile(path.join(__dirname, "sortedLowerLipsum.txt"), sortedLowerData)
    })
    .then(() => {
      return fs.promises.appendFile(path.join(__dirname, filenames), "sortedLowerLipsum.txt")
    })
    .then(() => {
      return fs.promises.readFile(path.join(__dirname, filenames))
    })
    .then((data) => {
      let fileArray = data.toString().split("\n")
      for (let index = 0; index < fileArray.length; index++) {
        fs.promises.unlink(path.join(__dirname, fileArray[index]))
          .catch((err) => {
            console.log(err)
          })
      }
    })
    .catch((err) => {
      console.log(err)
    })
}

module.exports = readManipulateLipsum