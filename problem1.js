const fs = require("fs")
const path = require("path")



function makeDirWithRAndomFiles(dir) {

  fs.promises
    .mkdir(path.join(__dirname, dir), { recursive: true })
    .then(() => {
      console.log("Directory Created")

      let files = Array(10).fill(0)
        .map((element, elementIndex) => {
          return `${elementIndex}.json`
        })
      return files
    })
    .then((files) => {

      for (let index = 0; index < files.length; index++) {
        fs.promises.writeFile(path.join(__dirname, dir, files[index]), JSON.stringify("India"))
          .then(() => {
            console.log(`${files[index]} created`)
          })
          .catch((err) => {
            console.log(err)
          })
      }
    })
    .catch((err) => {
      console.log(err)
    })
}

module.exports = makeDirWithRAndomFiles